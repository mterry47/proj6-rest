"""
Nose tests for test_brevet.py

Credit: I had help generating the test cases using this provided website for brevet calculation: https://rusa.org/octime_acp.html
"""
from acp_times import open_time, close_time

import nose    # Testing framework
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

def test_controle_zero():
    '''
    A controle at 0 shouldn't shift the start time, and close time should be 1 hour later
    '''
    assert open_time(0, 200, "2017-01-01 09:00") == "2017-01-01T09:00:00+00:00"
    assert close_time(0, 200, "2017-01-01 09:00") == "2017-01-01T10:00:00+00:00"
    
def test_max_speed_change():
    '''
    Up to 200 km, max speed is 34, from 200-300 it is 32. Test to make sure switch is made.
    '''
    assert open_time(250, 300, "2020-11-10 12:00") == "2020-11-10T19:27:22.941176+00:00"
    assert close_time(250, 300, "2020-11-10 12:00") == "2020-11-11T04:40:00+00:00"

def test_within_60():
    '''
    Within the first 60 kilometers, the close time is recorded differently (according to French alg).
    '''
    assert open_time(30, 200, "2020-11-10 12:00") == "2020-11-10T12:53:52.941176+00:00"
    assert close_time(30, 200, "2020-11-10 12:00") == "2020-11-10T14:30:00+00:00"

    assert open_time(60, 200, "2020-11-10 12:00") == "2020-11-10T13:46:45.882353+00:00"
    assert close_time(60, 200, "2020-11-10 12:00") == "2020-11-10T16:00:00+00:00"

def test_beyond_brev():
    '''
    The maximum value for a 600 km brevet is 720 km. Time at 720 should match time at 600.
    '''
    assert open_time(720, 600, "2020-11-10 12:00") == open_time(600,600,"2020-11-10 12:00")
    assert close_time(720, 600, "2020-11-10 12:00") == close_time(600, 600, "2020-11-10 12:00")

def test_below_bound():
    '''
    Negative values should not be accepted.
    '''
    assert open_time(-5,200,"2020-11-10 12:00") == ""
    assert close_time(-5,200,"2020-11-10 12:00") == ""    

def test_high_controle():
    '''
    The controle at 999 for a 1000 km brevet is the last point before time is pre-decided by race length.
    '''
    assert open_time(999, 1000, "2019-12-31 23:00") == "2020-01-02T08:03:52.941176+00:00"
    assert close_time(999, 1000, "2019-12-31 23:00") == "2020-01-04T01:55:42.569128+00:00"