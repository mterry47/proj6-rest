<html>
    <head>
        <title>CIS 322 Project 6: Brevet REST</title>
    </head>

    <body>
        <h1>List All Open/Close Times</h1>
        <ul>
            <?php
                $json = file_get_contents('http://laptop-service/listAll');
                $obj = json_decode($json);
    	        $open_close_times = $obj->result;
                $ctr = 0;
                foreach ($open_close_times as $time) {
                    $ctr = $ctr + 1;
                    echo "<li>$ctr)  Open: $time->open_time  Close: $time->close_time</li>";
                }
            ?>
        </ul>

        <h1>List Open Times Only</h1>
        <ul>
            <?php
                $json = file_get_contents('http://laptop-service/listOpenOnly');
                $obj = json_decode($json);
                $open_times = $obj->result;
                $ctr = 0;
                foreach ($open_times as $time) {
                    $ctr = $ctr + 1;
                    echo "<li>Controle number ($ctr) will open at: $time->open_time</li>";
                }
            ?>
        </ul>

        <h1>List Close Times Only</h1>
        <ul>
            <?php
                $json = file_get_contents('http://laptop-service/listCloseOnly');
                $obj = json_decode($json);
                $close_times = $obj->result;
                $ctr = 0;
                foreach ($close_times as $time) {
                    $ctr = $ctr + 1;
                    echo "<li>Controle number ($ctr) close at: $time->close_time</li>";
                }
            ?>
        </ul>

        <h1>List All Open/Close Times In CSV Format</h1>
        <ul>
            <?php
                $csv = file_get_contents('http://laptop-service/listAll/csv');
                $csv = str_replace('"','',$csv);
                $csv = str_replace('\n','<br/>',$csv);
                echo "$csv";
            ?>
        </ul>

        <h1>List Top 3 (if there are 3) Open Times In CSV Format</h1>
        <ul>
            <?php
                $csv = file_get_contents('http://laptop-service/listOpenOnly/csv?top=3');
                $csv = str_replace('"','',$csv);
                $csv = str_replace('\n','<br/>',$csv);
                echo "$csv";
            ?>
        </ul>
    </body>
</html>
